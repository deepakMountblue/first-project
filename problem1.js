

const getDetails=(data,id)=>{
    if(!Array.isArray(data)){
        return({
            err:true,
            msg:"First arg should be an array"
        })
    }
    if(!id){
        return({
            err:true,
            msg:"second argument should be an integer"
        })
    }
    if(!Number.isInteger(id)){
        return({
            err:true,
            msg:"Id should be a number"
        })
    }
    for(let i=0;i<data.length;i++){
     
        if(data[i].id===id){
            return(data[i])
        }
    }
    return({
        err:true,
        msg:"no data found with that id "+id
    })
}


module.exports=getDetails