
const lastCar=(data)=>{
    if(!Array.isArray(data)){
        return({
            err:true,
            msg:"There should an arg which is array"
        })
    }
    if(data.length===0){
        return({err:true,
            msg:"No last car found in the inventory"
        })
    }
    return(data[data.length-1])
}

module.exports=lastCar