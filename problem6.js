
const getBmwAndAudi=(data)=>{
    const filteredData=[]
    if(!Array.isArray(data)){
        return({
            err:true,
            msg:"There should an arg which is array"
        })
    }
    let flag=0
    for(let i=0;i<data.length;i++){
      if(!data[i].car_make){
        flag=1
       break;
      }
    }
    if(flag===1){
      return({
        err:true,
        msg:"Array does not have valid values"
      })
  }
    for(let i=0;i<data.length;i++){
        if(data[i].car_make==='BMW' || data[i].car_make==="Audi"){
            filteredData.push(data[i])
        }
    }
    return(filteredData)
}


module.exports=getBmwAndAudi