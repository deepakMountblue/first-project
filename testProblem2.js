var data=require('./data')
var lastCar=require('./problem2')

let x=lastCar()
if(x.err){
    console.log(x.msg)
}
else{
    console.log("Last car is a "+x.car_make+" "+x.car_model)
}

x=lastCar([])
if(x.err){
    console.log(x.msg)
}
else{
    console.log("Last car is a "+x.car_make+" "+x.car_model)
}

x=lastCar(data)
if(x.err){
    console.log(x.msg)
}
else{
    console.log("Last car is a "+x.car_make+" "+x.car_model)
}