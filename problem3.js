
const sortData= (data)=>{

      if(!Array.isArray(data)){
        return({
            err:true,
            msg:"There should an arg which is array"
        })
      }

    let flag=0
      for(let i=0;i<data.length;i++){
        if(!data[i].car_model){
          flag=1
         break;
        }
      }
      if(flag===1){
        return({
          err:true,
          msg:"Array does not have valid values"
        })
    }
    const sorted=data.sort((a,b)=>{
      
        if (a.car_model.toLocaleLowerCase() < b.car_model.toLocaleLowerCase()) return -1
        return a.car_model.toLocaleLowerCase() > b.car_model.toLocaleLowerCase() ? 1 : 0
      })
      
    return(sorted)
}


module.exports=sortData