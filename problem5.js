var getYear =require('./problem4')

const YearLessthan=(data,year)=>{
    if(!Array.isArray(data)){
        return({
            err:true,
            msg:"First arg should be an array"
        })
    }
    if(!year){
        return({
            err:true,
            msg:"second argument should be an integer"
        })
    }
    if(!Number.isInteger(year)){
        return({
            err:true,
            msg:"Year should be a number"
        })
    }
    let older=[]
    const yearData=getYear(data)
    if(yearData.err){
        return yearData
    }
    for(let i=0;i<yearData.length;i++){
        if(yearData[i]<year){
            older.push(yearData[i])
        }
    }

    return(older)
}



module.exports=YearLessthan