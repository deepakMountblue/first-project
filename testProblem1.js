var data=require('./data')

var getDetails=require('./problem1')
let x=getDetails(data,45)

if(x.err){
    console.log(x.msg)
}   
else{
    console.log(`Car ${x.id} is a ${x.car_year} ${x.car_make} ${x.car_model}`)
}

x=getDetails(500)

if(x.err){
    console.log(x.msg)
}   
else{
    console.log(`Car ${x.id} is a ${x.car_year} ${x.car_make} ${x.car_model}`)
}


x=getDetails(data,'20')

if(x.err){
    console.log(x.msg)
}   
else{
    console.log(`Car ${x.id} is a ${x.car_year} ${x.car_make} ${x.car_model}`)
}