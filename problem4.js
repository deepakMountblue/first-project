
const getYear=(data)=>{
    if(!Array.isArray(data)){
        return({
            err:true,
            msg:"There should an arg which is array"
        })
      }

    let flag=0
      for(let i=0;i<data.length;i++){
        if(!data[i].car_year){
          flag=1
         break;
        }
      }
      if(flag===1){
        return({
          err:true,
          msg:"Some array values does not have years in it "
        })
    }
    let Year=[]
    for(let i=0;i<data.length;i++){
        Year.push(data[i].car_year)
    }
    return(Year)
}

module.exports=getYear